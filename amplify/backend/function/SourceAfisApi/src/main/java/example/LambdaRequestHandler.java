

package example;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import com.machinezoo.sourceafis.FingerprintTemplate;
import com.machinezoo.sourceafis.FingerprintImage;
import com.machinezoo.sourceafis.FingerprintImageOptions;
import com.machinezoo.sourceafis.FingerprintMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.json.Json;
import javax.json.JsonReader;
import javax.json.JsonObject;
import java.io.StringReader;

public class LambdaRequestHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
  private Logger logger = LoggerFactory.getLogger(this.getClass());

  public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent request, Context context) {
//    logger.info("Hello slf4j Logger!");
    String body = request.getBody();
    JsonReader jsonReader = Json.createReader(new StringReader(body));
    JsonObject payload = jsonReader.readObject();

    String candidateImageUri = payload.getString("candidate");
    String probeImageUri = payload.getString("probe");

    String encodedCandidate = candidateImageUri.split("base64,")[1];
    String encodedProbe = probeImageUri.split("base64,")[1];

    byte[] candidateBytes = Base64.getDecoder().decode(encodedCandidate);
    byte[] probeBytes = Base64.getDecoder().decode(encodedProbe);

    double score = 0;
    int statusCode = 200;

    try {
      System.out.println("Creating candidate fingerprint image");
      FingerprintImage candidateFingerprintImage = new FingerprintImage(
        candidateBytes,
        new FingerprintImageOptions().dpi(500)
      );

      System.out.println("Creating probe fingerprint image");
      FingerprintImage probeFingerprintImage = new FingerprintImage(
        probeBytes,
        new FingerprintImageOptions().dpi(500)
      );

      System.out.println("Creating candidate fingerprint template");
      FingerprintTemplate candidate = new FingerprintTemplate(candidateFingerprintImage);

      System.out.println("Creating probe fingerprint template");
      FingerprintTemplate probe = new FingerprintTemplate(probeFingerprintImage);

      System.out.println("Performing match");
      score = new FingerprintMatcher(probe).match(candidate);
      System.out.println("score: " + score);
    } catch (Exception e) {
      System.out.println("Caught an exception");
      statusCode = 500;
    }

    JsonObject responsePayload = Json.createObjectBuilder()
      .add("score", score)
      .build();

    Map<String, String> headers = new HashMap<>();
    headers.put("Access-Control-Allow-Origin", "*");
    headers.put("Access-Control-Allow-Headers", "*");
    headers.put("Content-Type", "application/json");

    APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
    response.setStatusCode(statusCode);
    response.setHeaders(headers);
    response.setBody(responsePayload.toString());
    return response;
  }
}
